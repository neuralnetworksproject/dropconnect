# README #
Read me contains the description for all of the ipython notebooks that were run for the project

cifar_dropout_ragha.ipynb - The runs in the ipython notebook contain dropout results for cifar dataset

cifar_lenet_ragha.ipynb - The runs in the ipython notebook contain no drop results for cifar dataset


crop_noscale_dropconnect_ragha.ipynb - The runs in the ipython notebook contain dropconnect results for Mnist dataset with cropping


DropOutTanh.ipynb - The runs in the ipython notebook contain runs for dropout with MNSIT dataset with tanh as activation for the dropout layer,
It also contains the No drop SVHN experiments 


graphs_mnist.ipynb - This ippython notebook contains the graph plots representing various runs on the Mnist dataset

MLP2Layer_DropConnect_SVHN_Dataset.ipynb - The runs in the ipython notebook specify the various experiments performed on the SVHN dataset


mnist.ipynb - The runs in the ipython notebook contain the various experiments performed on the Mnist dataset

MNIST_NoCrop_NoScale_Fully150_p0.5.ipynb - experiments on Mnist, SVHN and Cifar datasets

MNIST_NoDrop_Crop_NOScale.ipynb - experiments on Mnist, SVHN and Cifar datasets

MNIST_NoDROP_NoCrop_NoScale -  experiments on Mnist, SVHN and Cifar datasets

MNIST_NoDROP.ipynb - experiments on Mnist, SVHN and Cifar datasets

mnistdropout.ipynb - experiments on Mnist, SVHN and Cifar datasets

nocrop_noscale_dropout_ragha.ipynb - experiments on Mnist, SVHN and Cifar datasets

NoDropDefaultSigmoid.ipynb - Experiments where Sigmoid is used as an activation function in the fully connected layer

NoDropDefaultTanh.ipynb - Experiments where tanh is used as an activation function in the fully connected layer

p_0.8_nocrop_noscale_dropconnect.ipynb - Dropconnect experiments

Testing.ipynb - Some basic test results performed

vchrome1.ipynb - Experiments on Mnist, SVHN Drop connect and Drop out 

vchrome2.ipynb - Mnist drop connect and SVHN drop out

vfirefox1.ipynb - Experiments on Mnist drop connect and drop out and SVHN drop connect

vfirefox2.ipynb - Experiments on Mnist drop out and SVHN drop connect

Voting.ipynb - Contains the implementation and the voting results for the SVHN and the Mnist dataset

vsafari1.ipynb - Experiments on Mnist MLP SVHN drop out

vsafari2.ipynb - Experiments on Mnist MLP and SVHN drop out

chrome2terminaloutputs - experiments on SVHN

safari1terminaloutputs - experiments on SVHN

safari2terminaloutputs - experiments on SVHN